(function($) {
  function styleLinkListBlock() {
    $('.link-list-block').first().wrap("<div class='colored-links'></div>");
    $('.link-list-block a').each(function() {
      var element = $(this);
      if (element.html().length > 12) {
        element.css('letter-spacing', '0px');
      }
    });
    $(".box-2 .link-list-block .block-title").click(function(){
      $(this).parent().children("ul").stop().slideToggle();
    });
  }

  $(document).ready(function() {
    styleLinkListBlock();
  });
})(jQuery);
